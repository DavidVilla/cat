// from http://www.javabeat.net/2012/06/creating-writing-reading-java-files-api-java-7/

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;

public class cat {
    public static void main(String[] args) {
	String filename = args[0];
	try {
	     Path file = Paths.get(filename);
	     BufferedReader reader = Files.newBufferedReader(
		  file, Charset.defaultCharset());

	     String line;
	     while((line = reader.readLine()) != null) {
		 System.out.println(line);
	     }
	 }
	catch(IOException exception) {
	    System.err.format("cat: %s: No such file or directory", filename);
	}
    }
}
